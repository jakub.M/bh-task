import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { ValidationPipe } from '@nestjs/common';
import { TypeOrmConfigService } from '../src/config/typeorm.service';
import { MockTypeOrmConfigService } from '../src/config/typrorm-test.service';

describe('EventController (e2e)', () => {
  let app;

  const newEvent = {
    firstName: 'John',
    lastName: 'Smith',
    email: 'joth.smith@example.com',
    date: '2020-07-24T12:27:26.000Z',
  }

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider(TypeOrmConfigService)
      .useValue(new MockTypeOrmConfigService())
      .compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe({ whitelist: true }));
    await app.init();
  });

  it('post with invalide vales', () =>
    request(app.getHttpServer())
      .post('/event')
      .send({
        firstName: 'J',
        lastName: 'S',
        email: 'joth.smith@',
        date: '12-12-2021',
      })
      .expect(400));

  it('post events', () =>
    request(app.getHttpServer())
      .post('/event')
      .send(newEvent)
      .expect(201));

  it('get events', () =>
    request(app.getHttpServer())
      .get('/event')
      .expect((res) => {
        res.body = res.body.map(({ id, ...rest }) => rest);
      })
      .expect(200, [newEvent]));
});
