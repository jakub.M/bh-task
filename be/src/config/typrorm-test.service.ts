import { TypeOrmModuleOptions, TypeOrmOptionsFactory } from '@nestjs/typeorm';

export class MockTypeOrmConfigService implements TypeOrmOptionsFactory {
  createTypeOrmOptions(): TypeOrmModuleOptions {
    return {
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '',
      database: 'event-test',
      synchronize: true,
      dropSchema: true,
      entities: [`src/**/*.entity{.ts,.js}`],
      keepConnectionAlive: true,
    };
  }
}
