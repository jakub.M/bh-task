import { Module } from '@nestjs/common';
import { EventController } from './event.controller';
import { CqrsModule } from '@nestjs/cqrs';
import { eventCommands } from './commands/event-commands';
import { eventQueries } from './queries/event-queries';

@Module({
  imports: [CqrsModule],
  controllers: [EventController],
  providers: [...eventCommands, ...eventQueries],
})
export class EventModule {}
