import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { GetEventsQuery } from '../impl/get-events.query';
import { Event } from '../../enties/event.entity';

@QueryHandler(GetEventsQuery)
export class GetEventsHandler implements IQueryHandler<GetEventsQuery> {
  execute = async () => await Event.find({});
}
