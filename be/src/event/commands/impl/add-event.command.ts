import { AddEventDto } from '../../dtos/add-event.dto';

export class AddEventCommand {
  constructor(public readonly event: AddEventDto) {}
}
