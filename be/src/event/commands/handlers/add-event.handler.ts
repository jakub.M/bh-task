import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { AddEventCommand } from '../impl/add-event.command';
import { Event } from '../../enties/event.entity';

@CommandHandler(AddEventCommand)
export class AddEventHandler implements ICommandHandler<AddEventCommand> {
  execute = async ({ event }: AddEventCommand) => {
    const newEvent = Object.assign(new Event(), event);
    await newEvent.save({ reload: false });
  };
}
