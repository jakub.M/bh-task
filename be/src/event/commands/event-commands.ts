import { EventsHandler } from '@nestjs/cqrs';
import { AddEventHandler } from './handlers/add-event.handler';

export const eventCommands = [AddEventHandler];
