import { IsEmail, MinLength, IsDateString } from 'class-validator';
export class AddEventDto {
  @MinLength(3)
  firstName: string;

  @MinLength(3)
  lastName: string;

  @IsEmail()
  email: string;

  @IsDateString()
  date: Date;
}
