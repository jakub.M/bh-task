import { Controller, Post, Body, Get } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { AddEventDto } from './dtos/add-event.dto';
import { AddEventCommand } from './commands/impl/add-event.command';
import { GetEventsQuery } from './queries/impl/get-events.query';

@Controller('event')
export class EventController {
  constructor(private commandBus: CommandBus, private queryBus: QueryBus) {}

  @Post()
  async addEvent(@Body() event: AddEventDto) {
    return await this.commandBus.execute(new AddEventCommand(event));
  }

  @Get()
  async getEvents() {
    return await this.queryBus.execute(new GetEventsQuery());
  }
}
