import React from 'react';
import { Backdrop, CircularProgress, makeStyles } from '@material-ui/core';
import { useSelector } from 'react-redux';

const useStyles = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}));

export const BackdropLoading = () => {
  const { backdrop } = useStyles();
  const loading = useSelector((state) => (state as any).loading);
  return (
    <Backdrop className={backdrop} open={loading} transitionDuration={{ exit: 500 }}>
      <CircularProgress {...{ size: 80, color: 'primary' }} />
    </Backdrop>
  );
};
