import React, { ReactNode } from 'react';
import { FormProvider, UseFormMethods } from 'react-hook-form';

export const FormWrapper = ({ methods, children, onSubmit }: Props) => (
  <FormProvider {...methods}>
    <form onSubmit={methods.handleSubmit(onSubmit)}>{children}</form>
  </FormProvider>
);

interface Props {
  methods: UseFormMethods<Record<string, any>>;
  children: ReactNode;
  onSubmit: (data: any) => void;
}
