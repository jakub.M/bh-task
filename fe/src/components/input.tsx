import React from 'react';
import Grid from '@material-ui/core/Grid/Grid';
import TextField from '@material-ui/core/TextField/TextField';
import { useFormContext } from 'react-hook-form';
import { getType, createLabel, error } from '../utils/input.utils';

export const Input = ({ sizes, name, label, type }: Props) => {
  const { errors, register } = useFormContext();
  return (
    <Grid item {...(sizes || { xs: 12 })}>
      <TextField
        variant='outlined'
        fullWidth
        {...{ name, ...getType(type), inputRef: register, ...createLabel(name, label), ...error(errors[name]), id: name }}
      />
    </Grid>
  );
};

interface Props {
  sizes?: Partial<
    Record<'xs' | 'sm' | 'md' | 'lg' | 'xl', boolean | 'auto' | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12>
  >;
  name: string;
  label?: string;
  type?: string;
}
