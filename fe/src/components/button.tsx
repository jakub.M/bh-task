import React from 'react';
import MaterialButton from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid/Grid';

export const Button = ({ text }: Props) => (
  <Grid item xs={12} >
    <MaterialButton type='submit' fullWidth variant='contained' color='primary'>
      {text}
    </MaterialButton>
  </Grid>
);

interface Props {
  text: string;
}
