import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import 'mutationobserver-shim';
import { EventForm } from '../views/event-form/componets/event-form';
import { act } from 'react-dom/test-utils';
import { format, addDays } from 'date-fns';

const mockPostEvent = jest.fn((data, onSuccess) => {
  onSuccess();
  return Promise.resolve(data);
});

describe('Event form', () => {
  it('should all fields render', () => {
    const { getAllByText } = render(<EventForm {...{ postEvent: mockPostEvent }} />);
    const firstNameLabels = getAllByText(/First Name/i);
    const lastNameLabels = getAllByText(/Last Name/i);
    const emailLabels = getAllByText(/Email/i);
    const dateLabels = getAllByText(/Date/i);
    expect(firstNameLabels).toHaveLength(2);
    expect(lastNameLabels).toHaveLength(2);
    expect(emailLabels).toHaveLength(2);
    expect(dateLabels).toHaveLength(2);
  });

  it('should display button', () => {
    const { getByText } = render(<EventForm {...{ postEvent: mockPostEvent }} />);
    const button = getByText(/Add Event/i);
    expect(button).toBeInTheDocument();
  });

  it('should dispaley required errors', async () => {
    const { getByText, findByText } = render(<EventForm {...{ postEvent: mockPostEvent }} />);
    const button = getByText(/Add Event/i);
    await act(async () => {
      fireEvent.submit(button);
    });
    expect(await findByText(/First name is required!/i)).toBeInTheDocument();
    expect(await findByText(/Last name is required!/i)).toBeInTheDocument();
    expect(await findByText(/Email is required!/i)).toBeInTheDocument();
  });

  it('should display invalid values', async () => {
    const { getByRole, getByText, findByText, getByLabelText } = render(<EventForm {...{ postEvent: mockPostEvent }} />);
    const firstNameInput = getByRole('textbox', { name: /First Name/i });
    const lastNameInput = getByRole('textbox', { name: /Last Name/i });
    const emailInput = getByRole('textbox', { name: /Email/i });
    const dateInput = getByLabelText(/date/i);
    const button = getByText(/Add Event/i);
    await act(async () => {
      fireEvent.input(firstNameInput, { target: { value: 'Jo' } });
      fireEvent.input(lastNameInput, { target: { value: 'Jo' } });
      fireEvent.input(emailInput, { target: { value: 'Jo' } });
      fireEvent.input(dateInput, { target: { value: '' } });
      fireEvent.submit(button);
    });
    expect(await findByText(/First name must be at least 3!/i)).toBeInTheDocument();
    expect(await findByText(/Last name must be at least 3!/i)).toBeInTheDocument();
    expect(await findByText(/mail must be a valid email!/i)).toBeInTheDocument();
    expect(await findByText(/Invalid date!/i)).toBeInTheDocument();
  });

  it('should reset values after success submit', async () => {
    const { getByRole, getByText, findByText, getByLabelText } = render(<EventForm {...{ postEvent: mockPostEvent }} />);
    const firstNameInput = getByRole('textbox', { name: /First Name/i });
    const lastNameInput = getByRole('textbox', { name: /Last Name/i });
    const emailInput = getByRole('textbox', { name: /Email/i });
    const dateInput = getByLabelText(/date/i);
    const button = getByText(/Add Event/i);
    await act(async () => {
      fireEvent.input(firstNameInput, { target: { value: 'John' } });
      fireEvent.input(lastNameInput, { target: { value: 'Smith' } });
      fireEvent.input(emailInput, { target: { value: 'john.smith@gmail.com' } });
      fireEvent.submit(button);
    });
    expect(firstNameInput).toHaveValue('')
    expect(lastNameInput).toHaveValue('')
    expect(emailInput).toHaveValue('')
    expect(dateInput).toHaveValue(format(addDays(new Date(), 1), 'yyyy-MM-dd'))
  })
});
