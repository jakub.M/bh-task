import React from 'react';
import { Input } from '../../../components/input';
import Grid from '@material-ui/core/Grid/Grid';
import { Button } from '../../../components/button';

export const EventFormBody = () => (
  <Grid container spacing={2}>
    <Input {...{ name: 'firstName', label: 'First name', sizes: { xs: 6 } }} />
    <Input {...{ name: 'lastName', label: 'Last name', sizes: { xs: 6 } }} />
    <Input {...{ name: 'email' }} />
    <Input {...{ name: 'date', type: 'date' }} />
    <Button {...{ text: 'Add Event' }} />
  </Grid>
);
