import React from 'react';
import { FormWrapper } from '../../../components/form-wrapper';
import { useForm } from 'react-hook-form';
import { EventFormBody } from './event-form-body';
import { yupResolver } from '@hookform/resolvers';
import { EventFormSchemaValidation } from '../../../utils/event-form.validation-schema';

export const EventForm = ({ postEvent }: Props) => {
  const methods = useForm({ resolver: yupResolver(EventFormSchemaValidation) });
  const onSuccess = methods.reset;
  const onSubmit = (data: any) => postEvent(data, onSuccess);
  return (
    <FormWrapper {...{ methods, onSubmit }}>
      <EventFormBody />
    </FormWrapper>
  );
};

interface Props {
  postEvent: (data: any, onSuccess: () => void) => void
}
