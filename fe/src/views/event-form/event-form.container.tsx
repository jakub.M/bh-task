import React from 'react';
import { useApi } from '../../utils/useApi';
import { EventForm } from './componets/event-form';
import { BackdropLoading } from '../../components/backdrop-loading';
import { transformData } from '../../utils/utils';

export const EventFormContainer = () => {
  const { api } = useApi();
  const postEvent = (data: any, onSuccess: () => void) => api('post', '/event', onSuccess, transformData(data));
  return (
    <>
      <EventForm {...{ postEvent }} />
      <BackdropLoading />
    </>
  );
};
