import { createStore } from 'redux';
import { loadingReducer } from './loading.reducer';

export const store = createStore(loadingReducer);
