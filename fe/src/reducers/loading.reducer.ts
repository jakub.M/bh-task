const defaulState = {
  loading: false,
};

export const loadingReducer = (state = defaulState, { type }: any) => {
  switch (type) {
    case 'start':
      return { ...state, loading: true };
    case 'end':
      return { ...state, loading: false };
    default:
      return state;
  }
};
