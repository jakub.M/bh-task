import React from 'react';
import Container from '@material-ui/core/Container/Container';
import { useStyles } from './styles';
import { EventFormContainer } from './views/event-form/event-form.container';
import { Provider } from 'react-redux';
import { store } from './reducers/store';

export const App = () => {
  const { paper } = useStyles();

  return (
    <Provider {...{ store }}>
      <Container component='main' maxWidth='sm'>
        <div className={paper}>
          <EventFormContainer />
        </div>
      </Container>
    </Provider>
  );
};
