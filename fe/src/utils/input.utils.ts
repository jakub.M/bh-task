import { startCase } from 'lodash';
import { format, addDays } from 'date-fns';

export const getType = (type?: string) =>
  type === 'date' ? { type, defaultValue: format(addDays(new Date(), 1), 'yyyy-MM-dd'), InputLabelProps: { shrink: true } } : {};

export const error = (error: any) => (error ? { error: true, helperText: error.message } : {});

export const createLabel = (name: string, label?: string) => ({ label: startCase(label || name) });
