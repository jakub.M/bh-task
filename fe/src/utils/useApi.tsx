import axios, { Method, AxiosResponse } from 'axios';
import cogoToast from 'cogo-toast';
import { handlerErr } from './error.handler';
import { useDispatch } from 'react-redux'

export const apiUrl = 'http://localhost:3001';

export const useApi = () => {
  const dispatch = useDispatch();
  const api = async (method: Method, path: string, onSuccess?: (res?: AxiosResponse<any>) => void, data?: any) => {
    dispatch({ type: 'start' })
    await axios({
      withCredentials: true,
      method,
      url: apiUrl + path,
      data,
      timeout: 4000,
    })
      .then((res) => {
        dispatch({ type: 'end' })
        cogoToast.success('Success');
        onSuccess?.(res);
      })
      .catch((err) => {
        dispatch({ type: 'end' })
        handlerErr(err?.response ?? 0);
      });
  };

  return { api };
};
