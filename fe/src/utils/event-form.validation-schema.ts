import { string, object, date } from 'yup';
import { format } from 'date-fns';

export const EventFormSchemaValidation = object().shape({
  firstName: string().required('First name is required!').min(3, 'First name must be at least 3!'),
  lastName: string().required('Last name is required!').min(3, 'Last name must be at least 3!'),
  email: string().required('Email is required!').email('Email must be a valid email!'),
  date: date().typeError('Invalid Date!').required('Date is required!').min(new Date(), `date must be later than ${format(new Date(), 'dd-MM-yyyy')}`),
});
