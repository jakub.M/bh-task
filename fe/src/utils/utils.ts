export const transformData = (data: any) => ({ ...data, date: new Date(data.date) });
