import { AxiosResponse } from 'axios';
import cogoToast from 'cogo-toast';

export const handlerErr = ({ status, data }: AxiosResponse) => {
  switch (status) {
    case 400:
      data?.message?.map?.((msg: string) => cogoToast.error(msg));
      break;
    case 500:
      cogoToast.error('Server error!');
      break;
    case 403:
      cogoToast.error('Access forebiden!');
      break;
    case 401:
      cogoToast.error('Login first!');
      break;
    default:
      cogoToast.error('Error');
  }
};
